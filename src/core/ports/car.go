package ports

import (
	"admin/src/core/domain"
)

type CarService interface {
	AddCar(car domain.Car, carType domain.CarType) error
	GetCarByPlate(plate string) (*domain.Car, error)
	UpdateCarByPlate(plate string, car domain.Car) error
	DeleteCarByPlate(plate string) error
	AddCarType(carType domain.CarType) error
	AddCarTypeCar(car domain.Car, carType domain.CarType) error
	GetCars() ([]domain.Car, error)
	GetCarInfo(plate string) (float32, *domain.Car, error)
	GetCarCost(car *domain.Car, carType string) float32
}

type CarRepository interface {
	AddCar(car domain.Car, carType domain.CarType) error
	GetCarByPlate(plate string) (*domain.Car, error)
	UpdateCarByPlate(plate string, car domain.Car) error
	DeleteCarByPlate(plate string) error
	AddCarType(carType domain.CarType) error
	AddCarTypeCar(car domain.Car, carType domain.CarType) error
	DelCarTypeCar(carPlate string) error
	GetCars() ([]domain.Car, error)
}

type CarDelivery interface {
	GetCarCost(car *domain.Car, carType string) float32
}
