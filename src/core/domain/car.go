package domain

type Car struct {
	VehicleRegistrationPlate string     `json:"plate" gorm:"primaryKey"`
	CarModel                 string     `json:"car_model" gorm:"not null"`
	ProductionYear           int        `json:"productionYear" gorm:"check: production_year > 1900"`
	Mileage                  int        `json:"mileage"`
	CarTypeCar               CarTypeCar `gorm:"foreignKey:Car"`
}

type CarTypeCar struct {
	ID      uint   `gorm:"primaryKey"`
	Car     string `gorm:"unique"`
	CarType string
}

type CarType struct {
	Name       string       `json:"name" gorm:"primaryKey"`
	CarTypeCar []CarTypeCar `gorm:"foreignKey:CarType; constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
}
