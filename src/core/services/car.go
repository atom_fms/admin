package services

import (
	"admin/src/core/domain"
	"admin/src/core/ports"
)

type CarService struct {
	repo ports.CarRepository
	del  ports.CarDelivery
}

func NewCarService(repo ports.CarRepository, del ports.CarDelivery) *CarService {
	return &CarService{
		repo: repo,
		del:  del,
	}
}

func (c *CarService) AddCar(car domain.Car, carType domain.CarType) error {
	return c.repo.AddCar(car, carType)
}

func (c *CarService) GetCarByPlate(plate string) (*domain.Car, error) {
	return c.repo.GetCarByPlate(plate)
}

func (c *CarService) UpdateCarByPlate(plate string, car domain.Car) error {
	return c.repo.UpdateCarByPlate(plate, car)
}

func (c *CarService) DeleteCarByPlate(plate string) error {
	return c.repo.DeleteCarByPlate(plate)
}

func (c *CarService) AddCarType(carType domain.CarType) error {
	return c.repo.AddCarType(carType)
}

func (c *CarService) AddCarTypeCar(car domain.Car, carType domain.CarType) error {
	return c.repo.AddCarTypeCar(car, carType)
}

func (c *CarService) GetCars() ([]domain.Car, error) {
	return c.repo.GetCars()
}

func (c *CarService) GetCarInfo(plate string) (float32, *domain.Car, error) {
	car, err := c.repo.GetCarByPlate(plate)
	carCost := c.del.GetCarCost(car, car.CarTypeCar.CarType)
	return carCost, car, err
}
