package delivery

import (
	"admin/src/adapters/delivery/proto"
	"admin/src/core/domain"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"os"
)

type CarCostGRPCDelivery struct {
	c proto.CarCostServiceClient
}

func NewCarCostGRPCDelivery() *CarCostGRPCDelivery {
	grpcPort := os.Getenv("GRPC_PORT")
	grpcHost := os.Getenv("GRPC_HOST")
	grpcAddr := fmt.Sprintf("%s:%s", grpcHost, grpcPort)
	conn, err := grpc.Dial(grpcAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}

	c := proto.NewCarCostServiceClient(conn)

	return &CarCostGRPCDelivery{
		c: c,
	}
}

func (c *CarCostGRPCDelivery) GetCarCost(car *domain.Car, carType string) float32 {
	req := &proto.GetCarCostRequest{Car: &proto.Car{Year: uint32(car.ProductionYear), Mileage: uint32(car.Mileage), Type: carType}}
	res, err := c.c.CarCost(context.Background(), req)

	if err != nil {
		log.Fatal(err)
	}

	return res.GetCarCost()
}
