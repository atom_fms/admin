package handler

import (
	"admin/src/core/domain"
	"admin/src/core/services"
	"fmt"
	"github.com/urfave/cli"
)

type CLIHandler struct {
	svc services.CarService
}

func NewCLIHandler(CarService services.CarService) *CLIHandler {
	return &CLIHandler{
		svc: CarService,
	}
}

func (c *CLIHandler) AddCar(ctx *cli.Context) error {
	car := domain.Car{
		VehicleRegistrationPlate: ctx.String("plate"),
		CarModel:                 ctx.String("model"),
		ProductionYear:           ctx.Int("year"),
		Mileage:                  ctx.Int("mileage"),
	}

	carType := domain.CarType{
		Name: ctx.String("type"),
	}

	err := c.svc.AddCar(car, carType)

	if err != nil {
		return err
	}
	return nil

}

func (c *CLIHandler) DelCar(ctx *cli.Context) error {
	plate := ctx.String("plate")

	err := c.svc.DeleteCarByPlate(plate)

	if err != nil {
		return err
	}
	return nil

}

func (c *CLIHandler) UpdateCar(ctx *cli.Context) error {
	plate := ctx.String("plate")

	car := domain.Car{
		VehicleRegistrationPlate: plate,
		CarModel:                 ctx.String("model"),
		ProductionYear:           ctx.Int("year"),
	}

	err := c.svc.UpdateCarByPlate(plate, car)

	if err != nil {
		return err
	}
	return nil

}

func (c *CLIHandler) GetCars(ctx *cli.Context) error {
	cars, err := c.svc.GetCars()

	if err != nil {
		return err
	}
	fmt.Println("All cars")
	for _, car := range cars {
		fmt.Printf("{"+
			"'plate': '%s', "+
			"'model': '%s', "+
			"'type': '%s', "+
			"'year': %d, "+
			"'mileage': %d}\n",
			car.VehicleRegistrationPlate,
			car.CarModel,
			car.CarTypeCar.CarType,
			car.ProductionYear,
			car.Mileage,
		)
	}

	return nil

}

func (c *CLIHandler) GetCar(ctx *cli.Context) error {
	price, car, err := c.svc.GetCarInfo(ctx.String("plate"))

	if err != nil {
		return err
	}
	fmt.Printf("{"+
		"'plate': '%s', "+
		"'model': '%s', "+
		"'type': '%s', "+
		"'year': %d, "+
		"'mileage': %d, "+
		"'price': %v}\n",
		car.VehicleRegistrationPlate,
		car.CarModel,
		car.CarTypeCar.CarType,
		car.ProductionYear,
		car.Mileage,
		price,
	)

	return nil

}
