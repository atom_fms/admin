package handler

import (
	"admin/src/core/domain"
	"admin/src/core/services"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"strconv"
)

type SocketCLIHandler struct {
	svc services.CarService
}

func NewSocketCLIHandler(CarService services.CarService) *SocketCLIHandler {
	return &SocketCLIHandler{
		svc: CarService,
	}
}

func (c *SocketCLIHandler) handleCommand(cmd Command) (string, error) {
	cmdMap := map[string]func(map[string]string) (string, error){
		"addCar":    c.AddCar,
		"getCars":   c.GetCars,
		"getCar":    c.GetCar,
		"delCar":    c.DelCar,
		"updateCar": c.UpdateCar,
	}
	data, err := cmdMap[cmd.Command](cmd.Args)
	if err != nil {
		return "", errors.New(fmt.Sprintf("%v", err))
	}

	return data, nil
}

func HandleConnection(c net.Conn, h *SocketCLIHandler) error {
	defer c.Close()

	buf := make([]byte, 1024)

	for {
		b, err := c.Read(buf)
		if err != nil {
			return err
		}
		var cmd Command
		err = json.Unmarshal(buf[:b], &cmd)
		if err != nil {
			return errors.New(fmt.Sprintf("%v", err))
		}

		data, err := h.handleCommand(cmd)
		if err != nil {
			return err
		}

		if data != "" {
			data += "\n"
			_, err := c.Write([]byte(data))
			if err != nil {
				return err
			}
		}
	}
}

type Command struct {
	Command string            `json:"command"`
	Args    map[string]string `json:"args"`
}

func (c *SocketCLIHandler) AddCar(ctx map[string]string) (string, error) {
	year, err := strconv.Atoi(ctx["year"])
	if err != nil {
		return "Car's not added", err
	}

	mileage, err := strconv.Atoi(ctx["mileage"])
	if err != nil {
		return "Car's not added", err
	}

	car := domain.Car{
		VehicleRegistrationPlate: ctx["plate"],
		CarModel:                 ctx["model"],
		ProductionYear:           year,
		Mileage:                  mileage,
	}

	carType := domain.CarType{
		Name: ctx["type"],
	}

	err = c.svc.AddCar(car, carType)

	if err != nil {
		return "Car's not added", err
	}

	return "Car's added", nil

}

func (c *SocketCLIHandler) DelCar(ctx map[string]string) (string, error) {
	plate := ctx["plate"]

	err := c.svc.DeleteCarByPlate(plate)

	if err != nil {
		return "Car's not deleted", err
	}
	return "Car's deleted", nil

}

func (c *SocketCLIHandler) UpdateCar(ctx map[string]string) (string, error) {
	plate := ctx["plate"]

	year, err := strconv.Atoi(ctx["year"])
	if err != nil {
		return "Car's not updated", err
	}

	car := domain.Car{
		CarModel:       ctx["model"],
		ProductionYear: year,
	}

	err = c.svc.UpdateCarByPlate(plate, car)

	if err != nil {
		return "Car's not updated", err
	}
	return "Car's updated", err

}

func (c *SocketCLIHandler) GetCars(ctx map[string]string) (string, error) {
	cars, err := c.svc.GetCars()

	if err != nil {
		return "Car error", err
	}

	data := "All cars\n"
	//fmt.Println("All cars")
	for _, car := range cars {
		data += fmt.Sprintf("{"+
			"'plate': '%s', "+
			"'model': '%s', "+
			"'type': '%s', "+
			"'year': %d, "+
			"'mileage': %d}\n",
			car.VehicleRegistrationPlate,
			car.CarModel,
			car.CarTypeCar.CarType,
			car.ProductionYear,
			car.Mileage,
		)
		//fmt.Printf("{"+
		//	"'plate': '%s', "+
		//	"'model': '%s', "+
		//	"'type': '%s', "+
		//	"'year': %d, "+
		//	"'mileage': %d}\n",
		//	car.VehicleRegistrationPlate,
		//	car.CarModel,
		//	car.CarTypeCar.CarType,
		//	car.ProductionYear,
		//	car.Mileage,
		//)
	}

	return data, nil

}

func (c *SocketCLIHandler) GetCar(ctx map[string]string) (string, error) {
	price, car, err := c.svc.GetCarInfo(ctx["plate"])

	if err != nil {
		return "Car error", err
	}
	data := fmt.Sprintf("{"+
		"'plate': '%s', "+
		"'model': '%s', "+
		"'type': '%s', "+
		"'year': %d, "+
		"'mileage': %d, "+
		"'price': %v}\n",
		car.VehicleRegistrationPlate,
		car.CarModel,
		car.CarTypeCar.CarType,
		car.ProductionYear,
		car.Mileage,
		price,
	)

	return data, nil

}
