package repository

import (
	"admin/src/core/domain"
	"errors"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

type CarPostgresRepository struct {
	db *gorm.DB
}

func NewCarPostgresRepository() *CarPostgresRepository {
	host := os.Getenv("DB_HOST")
	port := os.Getenv("DB_PORT")
	user := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbname := os.Getenv("DB_NAME")

	conn := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		host,
		port,
		user,
		dbname,
		password,
	)

	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN:                  conn,
		PreferSimpleProtocol: true,
	}), &gorm.Config{})

	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&domain.CarType{})
	db.AutoMigrate(&domain.Car{})
	db.AutoMigrate(&domain.CarTypeCar{})

	return &CarPostgresRepository{
		db: db,
	}
}

func (c CarPostgresRepository) AddCar(car domain.Car, carType domain.CarType) error {
	req := c.db.Create(&car)

	errCarType := c.AddCarType(carType)
	if errCarType != nil {
		return errCarType
	}

	if req.RowsAffected == 0 {
		return errors.New(fmt.Sprintf("car not added: %v", req.Error))
	}

	errAddCarTypeCar := c.AddCarTypeCar(car, carType)

	if errAddCarTypeCar != nil {
		return errAddCarTypeCar
	}

	return nil
}

func (c CarPostgresRepository) GetCarByPlate(plate string) (*domain.Car, error) {
	car := &domain.Car{}
	req := c.db.Preload("CarTypeCar").First(&car, "vehicle_registration_plate = ? ", plate)

	if req.RowsAffected == 0 {
		return nil, errors.New("car not found")
	}

	return car, nil
}

func (c CarPostgresRepository) UpdateCarByPlate(plate string, car domain.Car) error {
	carForUpdate, _ := c.GetCarByPlate(plate)

	req := c.db.Model(&carForUpdate).Updates(car)

	if req.RowsAffected == 0 {
		return errors.New("car not found")
	}

	return nil
}

func (c CarPostgresRepository) DeleteCarByPlate(plate string) error {
	carForDelete, _ := c.GetCarByPlate(plate)
	err := c.DelCarTypeCar(plate)

	if err != nil {
		return err
	}

	req := c.db.Delete(&carForDelete)

	if req.RowsAffected == 0 {
		return errors.New("car not found")
	}

	return nil
}

func (c CarPostgresRepository) AddCarType(carType domain.CarType) error {
	req := c.db.FirstOrCreate(&carType)

	if req.Error != nil {
		return errors.New(fmt.Sprintf("car type not added: %v", req.Error))
	}

	return nil
}

func (c CarPostgresRepository) AddCarTypeCar(car domain.Car, carType domain.CarType) error {
	carTypeCar := &domain.CarTypeCar{Car: car.VehicleRegistrationPlate, CarType: carType.Name}

	req := c.db.Create(carTypeCar)

	if req.Error != nil {
		return errors.New(fmt.Sprintf("carTypeCar not added: %v", req.Error))
	}

	return nil
}

func (c CarPostgresRepository) DelCarTypeCar(carPlate string) error {
	req := c.db.Where("car = ?", carPlate).Delete(&domain.CarTypeCar{})

	if req.RowsAffected == 0 {
		return errors.New(fmt.Sprintf("car not found: %v", req.Error))
	}

	return nil
}

func (c CarPostgresRepository) GetCars() ([]domain.Car, error) {
	var cars []domain.Car

	err := c.db.Model(&domain.Car{}).Preload("CarTypeCar").Find(&cars).Error

	if err != nil {
		return nil, errors.New(fmt.Sprintf("cars: %v", err))
	}

	return cars, nil
}
