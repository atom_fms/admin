package main

import (
	"admin/src/adapters/delivery"
	"admin/src/adapters/handler"
	"admin/src/adapters/repository"
	"admin/src/core/services"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/urfave/cli"
	"log"
	"net"
	"os"
)

var (
	cliHandler    *handler.CLIHandler
	socketHandler *handler.SocketCLIHandler
	svc           *services.CarService
)

func init() {
	godotenv.Load()
}

func main() {
	store := repository.NewCarPostgresRepository()
	grpcDel := delivery.NewCarCostGRPCDelivery()
	svc = services.NewCarService(store, grpcDel)

	init := os.Args[1]

	switch init {
	case "socket":
		InitSocketServer()
	default:
		InitCLI()
	}
}

func InitSocketServer() {
	socketHandler = handler.NewSocketCLIHandler(*svc)
	port := os.Getenv("PORT")
	addr := fmt.Sprintf(":%s", port)
	ln, _ := net.Listen("tcp", addr)
	for {
		conn, _ := ln.Accept()
		go handler.HandleConnection(conn, socketHandler)
	}
}

func InitCLI() {
	app := cli.NewApp()
	cliHandler = handler.NewCLIHandler(*svc)

	app.Commands = []cli.Command{
		{
			Name:   "addCar",
			Action: cliHandler.AddCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
				&cli.StringFlag{
					Name: "model",
				},
				&cli.StringFlag{
					Name: "type",
				},
				&cli.IntFlag{
					Name:  "year",
					Value: 2023,
				},
				&cli.IntFlag{
					Name:  "mileage",
					Value: 0,
				},
			},
		},
		{
			Name:   "delCar",
			Action: cliHandler.DelCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
			},
		},
		{
			Name:   "updateCar",
			Action: cliHandler.UpdateCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
				&cli.StringFlag{
					Name: "model",
				},
				&cli.StringFlag{
					Name: "type",
				},
				&cli.IntFlag{
					Name: "year",
				},
			},
		},
		{
			Name:   "getCars",
			Action: cliHandler.GetCars,
		},
		{
			Name:   "getCar",
			Action: cliHandler.GetCar,
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name: "plate",
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
